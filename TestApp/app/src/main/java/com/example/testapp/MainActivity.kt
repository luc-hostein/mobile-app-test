package com.example.testapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView


class MainActivity : AppCompatActivity(), View.OnClickListener{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button: Button = findViewById(R.id.button)
        var i = 0
        button.setOnClickListener {
            i++
            val resultTextView : TextView = findViewById(R.id.textView2)
            resultTextView.text = "Taper : $i"
        }


    }

    override fun onClick(p0: View?) {
    }
}
